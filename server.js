//Nous l'utiliserons pour développer l'API REST.
const express = require('express');
/* middleware Simple Express pour le téléchargement de fichiers. 
Il analyse les demandes multipart/form-data, extrait les fichiers si disponibles et 
les rend disponibles sous la propriété req.files */
const fileUpload = require('express-fileupload');
//middleware Express pour activer les requêtes CORS (Cross-Origin Resource Sharing).
const cors = require('cors');
//middleware qui analyse le corps de la requête entrante avant vos gestionnaires et le rend disponible sous la propriété req.body
const bodyParser = require('body-parser');
//middleware Node.js pour la journalisation des requêtes HTTP
const morgan = require('morgan');
/* Une bibliothèque JavaScript qui fournit des fonctions utilitaires pour les tableaux, 
les nombres, les objets, les chaînes, etc. */
const _ = require('lodash');

const app = express();
// activation de files upload
app.use(fileUpload({
    createParentPath: true
}));

//utilisation de ces middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));
//on rend le dossier public  '/public', express.static('public')
app.use('/uploads', express.static('uploads'));

//route pour le drag and drop

app.post('/upload-avatar-multiple', async (req, res) => {
    console.log(req.files)
    try {
        if (!req.files) {
            console.log("ici")
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            //utilisez le nom du champ input (i.e. "avatar") pour recuperer le fichier chargé
            let avatar = req.files.file;



            //utilisez la methode mv() pour placer le fichier dans un repertoire de chargement (i.e. "uploads")
            avatar.mv('./uploads/' + avatar.name);
            //si j'associe http://localhost:numeroPortBackend/uploads/+ avatar.name  = le lien à mettre dans la bd
            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                link: 'http://localhost:3000/uploads/' + avatar.name,
                data: {
                    name: avatar.name,
                    mimetype: avatar.mimetype,
                    size: avatar.size
                }
            });
        }
    } catch (err) {
        console.log(err)
        res.status(500).send(err);
    }
});

//une route pour appel extérieur d'un seul fichier mais en blob
app.post('/upload-avatar-blob', async (req, res) => {
    console.log(req.files)
    try {
        if (!req.files) {
            console.log("ici")
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            //utilisez le nom du champ input (i.e. "avatar") pour recuperer le fichier chargé
            let avatarFileFromBlob = req.files.avatar;

            //utilisez la methode mv() pour placer le fichier dans un repertoire de chargement (i.e. "uploads")
            avatarFileFromBlob.mv('./uploads/' + avatarFileFromBlob.name);
            //si j'associe http://localhost:numeroPortBackend/uploads/+ avatar.name  = le lien à mettre dans la bd
            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                link: 'http://localhost:3000/uploads/' + avatarFileFromBlob.name,
                data: {
                    name: avatarFileFromBlob.name,
                    mimetype: avatarFileFromBlob.mimetype,
                    size: avatarFileFromBlob.size
                }
            });
        }
    } catch (err) {
        console.log(err)
        res.status(500).send(err);
    }
});



//lancer le serveur sur un port
const port = process.env.PORT || 3000;



app.listen(port, () =>
    console.log(`App is listening on port ${port}.`)
);